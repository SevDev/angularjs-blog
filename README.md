# AngularJS-Blog
Une appli créée afin d'apprendre le framework AngularJS

## Liens
* [Cours OpenClassrooms](https://openclassrooms.com/fr/courses/4668271-developpez-des-applications-web-avec-angular)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
