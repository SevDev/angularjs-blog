import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  posts = [
    {
      title: "Premier post",
      content: "Ullamco in deserunt aute consequat quis qui est excepteur. Elit exercitation excepteur sit consequat dolore nisi minim.",
      loveIts: 0,
      created_at: new Date()
    },{
      title: "Wow !",
      content: "Ullamco incididunt consequat labore amet voluptate mollit excepteur consequat labore proident. Occaecat in laborum laboris proident et enim duis minim nostrud ipsum sunt laborum dolor elit. Laborum voluptate eiusmod ipsum esse laboris Lorem deserunt. Officia eiusmod qui cillum deserunt reprehenderit velit dolor amet cupidatat. Labore laborum adipisicing irure ullamco qui irure do exercitation enim culpa cupidatat.",
      loveIts: 0,
      created_at: new Date()
    },{
      title: "Super post",
      content: "La publication de données ou d'informations désigne, elle, la mise à disposition de celles-ci dans des environnements informatiques, en particulier sur des bases de données et sur Internet (publication en ligne, publication électronique). (source: Wikipédia, Publication)",
      loveIts: 0,
      created_at: new Date()
    },{
      title: "Toujours plus de Lorem Ipsum !",
      content: "Exercitation cupidatat nisi esse veniam qui non. Adipisicing ut mollit culpa nisi proident elit sit ad incididunt veniam anim in cillum eiusmod. Incididunt proident non consectetur officia et anim minim non commodo do et est nostrud. Lorem laboris pariatur nulla labore ea enim aute duis ea excepteur laboris pariatur laboris.",
      loveIts: 0,
      created_at: new Date()
    },
  ]
}
